# DevOps

## Home work 1

## Home work 2

1. Added Terraform directory
2. Added .gitignore file in that directory

В файле /terraform/.gitignore будут игнорироваться:

1. Файлы содержащие ignore
   > \!ignore!.*
2. Все файлы .tfvars .tfvars.json которые могут содержать sensitive информацию
   > *.tfvars
   > *.tfvars.json
3. Все файлы в дирректории /private, кроме open.tf
   > /private/*
   > !/private/open.tf
